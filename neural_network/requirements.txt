# -*- coding: utf-8 -*-
# Author: Jan Jakub Kubik <jakupkubik@gmail.com>

# not using TensorFlow 2.x because of https://github.com/tensorflow/tfjs/issues/2442#issuecomment-565673431
tensorflow==1.15            # neural network
tensorflow-datasets==2.1.0  # gigaword dataset
tensorboard==1.15.0         # TensorBoard for logs
tensorflowjs==1.4.0         # converting trained and save TensorFlow model to TensorFlow.js
numpy==1.16.4               # operations with dataset
