#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Jan Jakub Kubik <jakupkubik@gmail.com>

import re
from itertools import groupby
from string import punctuation

import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds


class Dataset:
    """IMDb dataset class.

    Class for preparation of IMDb dataset for training of neural network in TensorFlow.
    From IMDb corpus is created lexicon with sub words by TensorFlow
    `SubwordTextEncoder` class by its method `build_from_corpus`. Lexicon is
    saved for TensorFlow usage, adjusted and saved for TensorFlow.js.
    Lexicon is used for IMDb corpus tokenization. From IMDb corpus are created tuples
    of input and target sub word tokens, that are used for neural network training.

    Attributes:
        batch_size (int): Batch size for IMDb dataset preparation.

    """

    def __init__(self, train_dataset_size, batch_size, lex_dir):
        self.__batch_size = batch_size

        train_dataset, validate_dataset, test_dataset = self.__download_dataset(
            train_dataset_size
        )

        self.tokenizer = tfds.features.text.SubwordTextEncoder.build_from_corpus(
            self.__generator(train_dataset), target_vocab_size=6000
        )

        self.train_dataset = self.__prepare_dataset(train_dataset)
        self.validate_dataset = self.__prepare_dataset(validate_dataset)
        self.test_dataset = self.__prepare_dataset(test_dataset)

        lexicon_path = lex_dir + "lexicon"
        self.tokenizer.save_to_file(lexicon_path)  # save lexicon for TensorFlow
        self.__save_corpus_for_tfjs(lexicon_path)  # save lexicon for TensorFlow.js

    @staticmethod
    def __download_dataset(train_dataset_size):
        """Download gigaword dataset.

        Download gigaword dataset.
        Use 5% of training data (200k articles), full validation
        data (150k articles) and testing data (2k articles).
        """
        return tfds.load(
            name="gigaword",
            split=(f"train[:{train_dataset_size}%]", "test", "validation"),
        )

    @staticmethod
    def __clean_data(data):
        """Clean input data.

        Data are cleaned by continuously applying nested functions for cleaning.

        Args:
            data (str): Data for cleaning

        Returns:
            str: Cleaned data.

        """

        def remove_invalid_words(input_str):
            words = ["UNK", "-lrb-", "-rrb-"]
            for w in words:
                input_str = input_str.replace(w, "")
            return input_str

        def remove_numbers(input_str):
            return re.sub(r"\d+", "", input_str)

        def remove_spaces_before_punc(input_str):
            return re.sub(r"\s+([?!.'-_,])", r"\1", input_str)

        def remove_not_req_punctuation(input_str):
            punc = set(punctuation)
            punc -= {"?", "!", ".", "'", "-", "_", ","}

            for c in punc:
                input_str = input_str.replace(c, "")
            return input_str

        def remove_multiple_punc_and_spaces(input_text):
            """Remove multiple punctuation and spaces."""
            punc = set(punctuation)
            punc.add(" ")
            adjusted_text = []
            for k, g in groupby(input_text):
                if k in punc:
                    adjusted_text.append(k)
                else:
                    adjusted_text.extend(g)

            return "".join(adjusted_text)

        print("Before cleaning")
        print(data)

        data_without_invalid_words = remove_invalid_words(data)
        data_without_numbers = remove_numbers(data_without_invalid_words)
        data_without_punc_duplicities = remove_multiple_punc_and_spaces(
            data_without_numbers
        )
        data_without_req_punc = remove_not_req_punctuation(
            data_without_punc_duplicities
        )
        cleaned_data = remove_spaces_before_punc(data_without_req_punc)

        print("After cleaning")
        print(cleaned_data)

        return cleaned_data.lower()

    def __generator(self, dat):
        """ Create generator with cleaned data for lexicon building from it."""
        for article in dat:
            data = article["document"].numpy().decode()
            yield self.__clean_data(data)

    @staticmethod
    def __save_corpus_for_tfjs(lexicon_path):
        """Adjust created lexicon for TensorFlow.js and save it.

        Create valid json lexicon for TensorFlow.js from
        saved lexicon for TensorFlow and save it.

        """
        path_to_tf_corpus = lexicon_path + ".subwords"
        path_for_tfjs_corpus = lexicon_path + ".json"

        with open(path_to_tf_corpus) as f:
            valid_json = ""
            for i, line in enumerate(f):
                # jump over first 2 lines (comments)
                if i < 2:
                    continue

                if "&undsc" in line:  # special charater for underscore
                    line = line.replace("\&undsc", "\\\&undsc")
                else:
                    line = line.replace('"', '\\"')  #

                valid_json += (
                    '"' + line[1:-2] + '"' + ","
                )  # at the end of each line is `' ` ('+space)

        # remove last ',' add '[' and ']' for valid json and save it
        print("[" + valid_json[:-1] + "]", file=open(path_for_tfjs_corpus, "w"))

    def __prepare_dataset(self, reviews):
        """Prepare dataset for training.

        Clean dataset, tokenize by sub word tokenizer from TensorFlow,
        create tuples of input and target token, convert it to TensorFlow dataset
        format and split it to batches.

        Args:
            reviews (tensorflow.python.data.ops.dataset_ops.DatasetV1Adapter): Part of dataset for preparation for
                                                                               training, validating or testing.

        Returns:
            tensorflow.python.data.ops.dataset_ops.DatasetV1Adapter: Prepared dataset for TensorFlow neural network
                                                                     training, testing or validating.

        """
        source_tokens = []
        target_tokens = []

        for index, article in enumerate(reviews):
            data = article["document"].numpy().decode()
            data = self.__clean_data(data)

            tokens = self.tokenizer.encode(data)
            for inx in range(len(tokens) - 1):  # exclude last 1 because of indexing
                source_tokens.append(tokens[inx])
                target_tokens.append(tokens[inx + 1])

        source_tokens = np.expand_dims(source_tokens, 1)
        target_tokens = np.expand_dims(target_tokens, 1)

        prepared_dataset = tf.data.Dataset.from_tensor_slices(
            (source_tokens, target_tokens)
        )

        return prepared_dataset.batch(self.__batch_size, drop_remainder=True)
