/**
 * @file options for saving settings for formatting of
 * predicted sequence to chrome storage.
 * @author Jan Jakub Kubik <jakupkubik@gmail.com>
 * @module options
 */


/**
 * Restoring options for showing predicted sequence formatting.
 * If settings are saved in chrome storage then they are used otherwise
 * default options are used.
 */
function restore_formatting_options() {
    chrome.storage.sync.get(['sequence'], function (result) {

        if (result.sequence) {

            document.getElementById("sequence-family").value = result.sequence.family;
            document.getElementById("sequence-weight").value = result.sequence.weight;
            document.getElementById("sequence-size").value = result.sequence.size;
            document.getElementById('s_size').value = result.sequence.size + 'px';
            document.getElementById('sequence-len').value = result.sequence.len;
            document.getElementById('s_len').value = result.sequence.len;
            document.getElementById('color').value = result.sequence.color;
            document.getElementById('background-color').value = result.sequence.b_color;
            document.getElementById("time_interval").value = result.sequence.t_interval;
            document.getElementById('t_interval').value = result.sequence.t_interval + 'px';

            console.log('Sequence settings are initialize by saved values.');
        } else { // default values

            reset_formatting_options()
        }
    });
}


/**
 * Resetting sequence formatting to default
 * options and saving it to chrome storage.
 */
function reset_formatting_options() {
    document.getElementById("sequence-family").value = 'Arial';
    document.getElementById("sequence-weight").value = 'Normal';
    document.getElementById("sequence-size").value = '20';
    document.getElementById('s_size').value = '20px';
    document.getElementById('sequence-len').value = '5';
    document.getElementById('s_len').value = '5';
    document.getElementById('color').value = '#C0C0C0'; // silver
    document.getElementById('background-color').value = '#FFFFFF'; // white
    document.getElementById("time_interval").value = '500';
    document.getElementById("t_interval").value = '500ms';
    console.log('Sequence settings are initialize by default values');

    save_formatting_options()
}


/**
 * Saving formatting options to chrome storage for formatting predicted sequence.
 */
function save_formatting_options() {
    let s_family = document.getElementById("sequence-family");
    s_family = s_family.options[s_family.selectedIndex].value;
    let s_weight = document.getElementById("sequence-weight");
    s_weight = s_weight.options[s_weight.selectedIndex].value;
    let s_size = document.getElementById('sequence-size').value;
    let s_len = document.getElementById('sequence-len').value;
    let s_color = document.getElementById('color').value;
    let b_color = document.getElementById('background-color').value;
    let t_interval = document.getElementById('time_interval').value;


    let obj = {};
    obj['sequence'] = {
        'family': s_family,
        'weight': s_weight,
        'size': s_size,
        'len': s_len,
        'color': s_color,
        'b_color': b_color,
        't_interval': t_interval,

    };

    chrome.storage.sync.set(obj, function() {
        console.log('All sequence properties are saved: ', obj);
    });
}


/**
 *  Listener for showing actual options for sequence formatting.
 *  It listen for DOMContentLoaded.
 */
document.addEventListener("DOMContentLoaded", restore_formatting_options);


/**
 * Listener for changing text if moving with slider.
 * It listen for input on slider for sequence size.
 */
document.getElementById("sequence-size")
    .addEventListener("input", function() {
        document.getElementById('s_size').value =
        document.getElementById("sequence-size").value+'px';
        console.log(document.getElementById("sequence-size").value);
    });


/**
 * Listener for changing text if moving with slider.
 * It listen for input on slider for sequence length.
 */
document.getElementById("sequence-len")
    .addEventListener("input", function() {
        document.getElementById('s_len').value =
        document.getElementById("sequence-len").value;
        console.log(document.getElementById("sequence-len").value);
    });


/**
 * Listener for changing text if moving with slider.
 * It listen for input on slider for time interval.
 */
document.getElementById("time_interval")
    .addEventListener("input", function() {
        document.getElementById('t_interval').value =
        document.getElementById("time_interval").value+'ms';
        console.log(document.getElementById("time_interval").value);
    });


/**
 * Listener for saving options for sequence formatting.
 * It listen for click on save button.
 */
document.getElementById("save_sequence_properties")
    .addEventListener("click", save_formatting_options);


/**
 * Listener for resetting options for sequence formatting.
 * It listen for click on reset button.
 */
document.getElementById("reset_sequence_properties")
    .addEventListener("click", reset_formatting_options);
