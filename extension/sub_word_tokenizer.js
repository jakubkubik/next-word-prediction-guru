/**
 * @file SubWordTokenizer class for encoding of words to numbers (tokens ids)
 * and decoding of numbers (token ids) to words.
 * Sub word tokenizer is rewritten part of sub word tokenizer from TensorFlow
 * (reference - https://github.com/tensorflow/datasets/blob/v3.0.0/tensorflow_datasets/core/features/text/subword_text_encoder.py#L41).
 * @author Jan Jakub Kubik <jakupkubik@gmail.com>
 * @module sub_word_tokenizer
 */


/**
 * This class is for sub word tokenization in TensorFlow.js.
 * It is based on TensorFlow sub word tokenization class.
 */
class SubWordTokenizer {
    /**
     * Constructor for sub word tokenizer class. It downloads lexicon from server
     * and creates mapping id to sub words and reverse. At the end it creates
     * cache for speeding up sub word tokenization process.
     * @constructor
     */
    constructor(id_to_subword, subword_to_id) {
        this.id_to_sub_word = id_to_subword;
        this.sub_word_to_id = subword_to_id;
        /** Underscore constant. */
        this.UNDERSCORE_REPLACEMENT = '\\\\&undsc';
        /** Cache size for token to id. */
        this.cache_size = 2**20;
        /** Cache for token to id. */
        this.token_to_ids_cache = Array(this.cache_size).fill([null, null])
    }

    /**
     * Main method for encoding of one string to sub part token ids.
     * @param {string} string - String for encoding.
     * @returns {Array<number>} - Array of sub part token ids.
     */
    encode(string){
        let tokens = this.tokenize(string);
        tokens = this.prepare_tokens_for_encode(tokens);

        let ids = [];
        for (let i=0; i<tokens.length; i++) {

            ids = ids.concat(this.token_to_ids(tokens[i]));
        }

        console.log('===========================================');
        console.log('Tokenized input text: ', tokens, ids);
        return ids
    }

    /**
     * Main method for decoding array of token ids to string tokens.
     * @param {Array<number>} token_ids - Array of id token for decoding.
     * @returns {string} - Decoded string.
     */
    decode(token_ids) {
        let decoded_text = '';
        for (let token_id of token_ids) {
            let lexicon_size;
            let offset = lexicon_size = Object.keys(this.id_to_sub_word).length;

            // if last char in token is '_' replace it with space
            if (token_id < lexicon_size) {

                let decoded_token = this.id_to_sub_word[token_id];
                if (decoded_token.slice(-1) === '_') {

                    decoded_token = decoded_token.substring(0, decoded_token.length - 1) + ' ';
                }

                decoded_text += decoded_token;
            } else {

                decoded_text += String.fromCharCode(token_id - offset -1);
            }
        }
        return decoded_text;
    }

    /**
     * Tokenization of input string to words.
     * @param {string} str - string for tokenization.
     * @returns {Array<string>} - Array of word strings.
     */
    tokenize(str) {
        let reserved_token = '\\\\&undsc';
        let non_word_re = new RegExp('(\\W+)');
        let spec_re = new RegExp('(\\\\&undsc)');

        let sub_words = str.split(spec_re);

        let tokens = [];
        for (let sub_word of sub_words) {

            (sub_word === reserved_token) ?
                tokens.push(sub_word):
                tokens = tokens.concat(sub_word.split(non_word_re));
        }

        // remove '' from tokens
        return tokens.filter(function (token) {
            return token !== '';
        });
    }

    /**
     * Prepare tokens for encoding.
     * @param {Array<string>} tokens - Array of tokens for preparation.
     * @returns {Array<string>}} - Array of prepared tokens for encoding.
     */
    prepare_tokens_for_encode(tokens) {
        let skip_next = false;

        function prepare_token(t, next_t){
            // If next token is a single space, add _ suffix
            // to token and skip the empty space.
            if (next_t === ' ') {
                t += '_';
                skip_next = true;
            }
            return [t, skip_next]
        }

        let prepared_tokens = [];
        let next_tokens = tokens.slice(1).concat([null]);
        let skip_single_token = false;

        for (let i = 0; i<tokens.length; i++) {

            if (skip_single_token) {
                skip_single_token = false;
                continue;
            }
            if (tokens[i] === this.UNDERSCORE_REPLACEMENT) {

                let t1 = this.UNDERSCORE_REPLACEMENT.slice(0, 3);
                let t2 = this.UNDERSCORE_REPLACEMENT.slice(3);
                t1 = prepare_token(t1, null)[0];
                t2 = prepare_token(t2, next_tokens[i])[0];
                prepared_tokens.push(t1);
                prepared_tokens.push(t2);
                continue;
            }

            let res = prepare_token(tokens[i], next_tokens[i]);
            let token = res[0];
            skip_single_token = res[1];
            prepared_tokens.push(token)
        }
        return prepared_tokens
    }

    /**
     * Simple function for hashing.
     * Copied from - https://www.measurethat.net/Benchmarks/Show/7018/0/string-hashcode
     * @param {string} str - String token for hashing.
     * @return {number} - Its hash.
     */
    hash(str) {
        let hash = 5381,
            i    = str.length;

        while(i) {
            hash = (hash * 33) ^ str.charCodeAt(--i);
        }

        /* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
         * integers. Since we want the results to be always positive, convert the
         * signed int to an unsigned by doing an unsigned bitshift. */
        return hash >>> 0;
    }

    /**
     * Map token to ids using lexicon.
     * @param {string} token - Token for mapping.
     * @returns {Array<number>} - Array of token's id.
     */
    token_to_ids(token) {
        // try to get from cache (hashing)
        let cache_location = this.hash(token) % this.cache_size;
        let result = this.token_to_ids_cache[cache_location];

        if (result[0] === token) {

            console.log('Got from cache.');
            return result[1]
        }


        let sub_words = this.token_to_sub_words(token);
        let ids = [];
        let lexicon_len = Object.keys(this.sub_word_to_id).length;

        let sub_word;
        for (sub_word of sub_words) {

            if (sub_word === this.UNDERSCORE_REPLACEMENT) {

                //lexicon_len is length of dictionary
                ids.push(lexicon_len + "_".charCodeAt());
                continue;
            }
            let sub_word_id = this.sub_word_to_id[sub_word];

            ids.push((!sub_word_id) ? lexicon_len + sub_word.charCodeAt() +1: sub_word_id + 1);
        }

        // Update cache
        this.token_to_ids_cache[cache_location] = [token, ids];

        return ids
    }
    /**
     * Split one token into multiple sub tokens if it is required.
     * @param token - Token to be splitted.
     * @returns {Array<string>} - Array of sub token.
     */
    token_to_sub_words(token){
        // Greedily split token into sub words.

        let sub_words = [];

        let start = 0;
        let max_sub_word_len = 15; // the longest sub word from lexicon

        while (start < token.length) {

            let sub_word = null;
            for (let end  = Math.min(token.length, start + max_sub_word_len) -1; end >= 0; end--) {

                let candidate = token.slice(start, end);

                if (this.sub_word_to_id[candidate] || candidate === this.UNDERSCORE_REPLACEMENT) {
                    sub_word = candidate;
                    sub_words.push(sub_word);
                    start = end;
                    break
                }
            }
            // No sub word match found. Append 1 character
            if (!sub_word) {
                sub_words.push(token[start]);
                start += 1;
            }
        }
        // remove '' from tokens
        return sub_words.filter(function (subword) {
            return subword !== '';
        });
    }
}
